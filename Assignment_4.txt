USE movies;

MOVING DATA TO HDFS:
hdfs dfs -put movies.csv/
hdfs dfs -put ratings.csv/
hdfs dfs -put users.csv/
hdfs dfs -put occupations.csv/


movies = spark.read\
.option(“inferSchema”, “true”)\
.option(“header”, “true”)\
.csv(“/user/Hadoop/movies/csv/movies.csv”)

ratings = spark.read\
.option(“inferSchema”, “true”)\
.option(“header”, “true”)\
.csv(“/user/Hadoop/ratings/csv/ratings.csv”)

users = spark.read\
.option(“inferSchema”, “true”)\
.option(“header”, “true”)\
.csv(“/user/Hadoop/users/csv/users.csv”)

occupations = spark.read\
.option(“inferSchema”, “true”)\
.option(“header”, “true”)\
.csv(“/user/Hadoop/occupations/csv/occupations.csv”)


1.Print a list of the 10 movies that received the most number of ratings.

query = spark.sql(“SELECT TOP 10
                   CAST(mov.tile AS nvarchar(30)) AS title
                   FROM movies mov
                   INNER JOIN ratings rat ON rat.movie_id=mov.id
                   GROUP BY mov.title”)
query.show()

2.Print a list of the 10 movies that received the most number of ratings, sorted by the number
of ratings.

query = spark.sql(“SELECT TOP 10
                   CAST(mov.tile AS nvarchar(30)) AS title
                   FROM movies mov
                   INNER JOIN ratings rat ON rat.movie_id=mov.id
                   GROUP BY mov.title;
                   ORDER BY ratings DESC”)
query.show()


3.Print a list of the genre of the top 10 most rated movies.

query = spark.sql(“SELECT TOP 10
                   gen.genre, AVG(rating) AS rating
                   FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   INNER JOIN users ON rat.user_id=users.id
                   INNER JOIN moviegenres movgen ON movgen.genre_id=genre.id
                   GROUP BY mov.genre”)
query.show()

4.Find the most rated movie by users in the age group 20 to 25.

query = spark.sql(“SELECT mov.title, AVG(rating) AS rating
                   FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   INNER JOIN users usr ON rat.user_id=usr.id
                   WHERE usr.age BETWEEN 20 AND 25
                   GROUP BY mov.title
                   ORDER BY ratings ASC”)
query.show()

5.Print the list of movies that were rated within a specific period of time of your choosing.

query = spark.sql(“SELECT mov.title FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   INNER JOIN users usr ON rat.user_id=usr.id
                   WHERE date_part(‘year’, mov.release_date) BETWEEN 1992 AND 1995
                   GROUP BY mov.title”)

query.show()

6.Print a list of the number of ratings received by each genre.

query = spark.sql(“SELECT gen.genre, AVG(rating) AS rating
                   FROM genres gen
                   INNER JOIN moviegenre movgen ON movgen.genre_id=genre.id
                   INNER JOIN ratings rat ON rat.movie_id=movie.id
                   GROUP BY gen.genre”)
query.show()

7.Find the most rated movie by users in the age group 20 to 25.
SELECT mov.title, AVG(rating) AS rating

query = spark.sql(“FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   INNER JOIN users usr ON rat.user_id=usr.id
                   WHERE usr.age BETWEEN 20 AND 25
                   GROUP BY mov.title
                   ORDER BY ratings ASC”)
query.show()

8.Print the title of the movie that was rated the most by students.

query = spark.sql(“SELECT TOP 1 mov.title
                   FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   INNER JOIN occupations occ ON rat.occupation_id=occ.id
                   WHERE rat.occupation_id=19
                   GROUP BY mov.title
                   ORDER BY ratings DESC”)
query.show()

9.Print the list of movies that received the highest number of “5” rating.

query = spark.sql(“SELECT mov.title
                   FROM ratings rat
                   INNER JOIN movies mov ON rat.movie_id=mov.id
                   WHERE mov.rating=5
                   GROUP BY mov.tile”)
query.show()


