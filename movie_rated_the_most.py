from mrjob.job import MRJob
from mrjob.step import MRStep
from operator import itemgetter, attrgetter
class CountMovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_movies,
                   reducer=self.reducer_count_movie_ratings),
            MRStep(reducer=self.reducer_sorted_output )
        ]
    def sortSecond(val):
        return val[5:]
    def mapper_movies(self, _, line):
        (user_id, movie_id, rating, timestamp) = line.split('\t')
        yield movie_id, 1
    def reducer_count_movie_ratings(self, key, values):
        yield 1, str(sum(values)).zfill(5)+key
    def reducer_sorted_output(self, count, movies):
        movies = sorted(movies, reverse=True)
        sort_based_array = []
        for movie in movies[0:10]:  sort_based_array.append(movie)
        sort_based_array = sorted(sort_based_array, key=lambda item:int(item[5:]), reverse=True)
        for sort_based_item in sort_based_array[0:1]:  yield int(sort_based_item[0:5]), int(sort_based_item[5:])

if __name__ == '__main__':
    CountMovieRatings.run()
