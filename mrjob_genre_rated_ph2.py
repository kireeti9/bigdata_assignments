from mrjob.job import MRJob
from mrjob.step import MRStep

genre = ["Unknown", "Action", "Adventure", "Animation", "Children", "Comedy", "Crime", "Documentary", "Drama", "Fantasy", "Film Noir", "Horror", "Musical", "Mystery", "Romance", "Sci-Fi", "Thriller", "War", "Western"]

class GetTitle(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_1
                   ,reducer=self.reducer_1
                   )
            ,MRStep(mapper=self.mapper_2 ,reducer=self.reducer_2)
            ,MRStep(mapper=self.mapper_3 ,reducer=self.reducer_3)
        ]

    def mapper_1(self, _, line):
        film_genre = None
        lst = line.split('|')
        if len(lst) == 24:
            for i in range(5,24):
                if lst[i]=='1':
                    film_genre = genre[i-5]
                    break
            movie_id, movie_title, date = lst[0], lst[1], lst[2]
            yield movie_id, ("", movie_title, film_genre)
        else:
            lst = line.split('\t')
            movie_id, count = lst[0], lst[1]
            yield movie_id, (count, "", "")

    def reducer_1(self, key, values):
        name_ = None
        genre_ = None
        for count, movie_title, film_genre in values:
            if movie_title:
                name_ = movie_title
                genre_ = film_genre
            else:
                yield int(count), (name_, genre_)

    def mapper_2(self, key, values):
        name, genre = values
        yield genre, key

        def reducer_2(self, key, values):
            yield key, sum(values)
    def mapper_3(self, key, value):
        yield 1, str(value).zfill(5) + key
    def reducer_3(self, key, values):
        values = sorted(values, reverse=True)
        for val in values:
            yield val[5:], int(val[0:5])

if __name__ == '__main__':
    GetTitle.run()


