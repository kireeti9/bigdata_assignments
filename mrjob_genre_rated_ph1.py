from mrjob.job import MRJob
from mrjob.step import MRStep

class CountMovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_students_only
                   ,reducer=self.reducer1
                   )
            ,MRStep(reducer=self.reducer2)
        ]
    def mapper_students_only(self, _, line):
        lst = line.split('|')
        if len(lst) == 5:  #u.user file
            user_id, age, gender, occupation = lst[0], lst[1], lst[2], lst[3]
            yield int(user_id), ("", "", occupation)
        else:  #u.data
            lst = line.split('\t')
            user_id_, movie_id, rating = lst[0], lst[1], lst[2]
            yield int(user_id_), (movie_id, rating, "")

    def reducer1(self, key, values):
        occ = None
        for movie_id, rating, occupation in values:
            if occupation == "student":
                occ = "student"
            else :
                if occ=="student": yield movie_id, 1

    def reducer2(self, key, values):
        yield int(key), sum(values)

if __name__ == '__main__':
    CountMovieRatings.run()



