from mrjob.job import MRJob
from mrjob.step import MRStep

class CountMovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_1
                   ,reducer=self.reducer_1
                   )
            ,MRStep(reducer=self.reducer_2)
            ,MRStep(reducer=self.reducer_3)
        ]
    def mapper_1(self, _, line):
        lst = line.split('\t')
        if len(lst) == 2:  # out_phase_1
            movie_id, count = lst[0], lst[1]
            yield movie_id, ("", int(count))
        else:  # file u.item
            lst = line.split('|')
            genre = [0] * 19
            movie_id, title, release_date  = lst[0], lst[1], lst[2]
            for i in range(0,18):
                genre[i] = lst[5+i]
            yield movie_id, (genre, "")
    def reducer_1(self, key, values):
        cnt = None
        for genre, count in values:
            if count:
                cnt = count
            elif cnt:
                for i in range(0,18):
                    yield i, int(cnt) * int(genre[i])

    def reducer_2(self, key, values):
        yield 1, str(sum(values)).zfill(5)+ str(key)
    def reducer_3(self, key, values):
        values = sorted(values, reverse=True)
        for val in values:
            yield int(val[5:]), int(val[0:5])

if __name__ == '__main__':
    CountMovieRatings.run()

