from mrjob.job import MRJob
from mrjob.step import MRStep

class CountMovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_movies,
                   reducer=self.reducer_count_movie_ratings),
            MRStep(reducer=self.reducer_sorted_output )
        ]
    def mapper_movies(self, _, line):
        (user_id, movie_id, rating, timestamp) = line.split('\t')
        yield movie_id, 1
    def reducer_count_movie_ratings(self, key, values):
        yield 1, str(sum(values)).zfill(5)+key
    def reducer_sorted_output(self, count, movies):
        movies = sorted(movies,  reverse=True)
        for movie in movies[0:10]:
            yield int(movie[0:5]), int(movie[5:])
if __name__ == '__main__':
    CountMovieRatings.run()
