from mrjob.job import MRJob
from mrjob.step import MRStep
class CountMovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_1
                   ,reducer=self.reducer_1
                   )
        ]
    def mapper_1(self, _, line):
        lst = line.split('\t')
        movie_id = lst[1]
        yield int(movie_id), 1
    def reducer_1(self, key, values):
        yield key, sum(values)
if __name__ == '__main__':
    CountMovieRatings.run()
