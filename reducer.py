# import sys
# current_word = None
# current_count = 0
# word = None
# for line in sys.stdin:
#     line = line.strip()
#     word, count = line.split('\t')
#     count = int(count)
#     if current_word == word:
#         current_count += count
#     else:
#         if current_word:
#             print('({},{})'.format(current_word, current_count))
#             current_count = count
#         current_word = word
# print('({},{})'.format(current_word, current_count) )

import sys
current_movie = None
current_count = 0
movie = None
for line in sys.stdin:
    line = line.strip()
    movie, count = line.split(' ')
    count = int(count)
    if current_movie == movie:
        current_count += count
    else:
        if current_movie:
            print('({},{})'.format(current_movie, current_count))
        current_count = count
        current_movie = movie
print('({},{})'.format(current_movie, current_count) )
